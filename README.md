# **Story 10 PPW: Webservice**

## Status pipelines
[![pipeline status](https://gitlab.com/lailasaffa/ppw-unittest/badges/master/pipeline.svg)](https://gitlab.com/lailasaffa/ppw-unittest/commits/master)


## Coverage Report
[![coverage report](https://gitlab.com/lailasaffa/ppw-unittest/badges/master/coverage.svg)](https://gitlab.com/lailasaffa/ppw-unittest/commits/master)

## Link Herokuapp
http://story-6-saffa.herokuapp.com/subscribers/


