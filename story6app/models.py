from django.db import models

# Create your models here.
class MyForm(models.Model):
    status = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now_add=True)

class SubscriberModels(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True, db_index=True)
    password = models.CharField(max_length=15)