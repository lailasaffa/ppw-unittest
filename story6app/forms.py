from django import forms
from datetime import datetime

class StatusForm(forms.Form):
    error_messages={
        'required':'This field is required.',
    }
    stat_attrs={
        'type':'text',
        'class':'form-control',
        'placeholder':'How are you doing today?',
        'id':'form-forms.py'
    }
    status=forms.CharField(
        label="Status", 
        max_length=300,
        required=True,
        widget=forms.TextInput(attrs=stat_attrs))

class SubscriberForms(forms.Form):
    error_messages={
        'required':'Required Field'
    }
    name_attrs={
        'type':'text',
        'class':'form-control',
        'placeholder': 'Required'
    }
    email_attrs={
        'type':'email',
        'class': 'form-control',
        'placeholder':'Required'
    }
    password_attrs={
        'type':'password',
        'class':'form-control',
        'placeholder':'Required'
    }

    name = forms.CharField(
        label='Name',
        required = True, 
        widget=forms.TextInput(attrs=name_attrs)
    )

    email = forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs)
    )

    password = forms.CharField(
        label='Password',
        required=True,
        widget=forms.PasswordInput(attrs=password_attrs)
    )