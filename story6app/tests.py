from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
from django.utils.encoding import force_text
import json

from .views import index
from .views import profile
from .views import books
from .views import subscribers
from .models import MyForm
from .models import SubscriberModels
from .forms import StatusForm
from .forms import SubscriberForms

# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from selenium.common.exceptions import NoSuchElementException

# Unit Test for Story 6 PPW 2018
class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response=Client().get('/story6/')
        self.assertEqual(response.status_code,200)

    def test_story6_using_index_func(self):
        found=resolve('/story6/')
        self.assertEqual(found.func,index)
        
    def test_story6_contain_string(self):
        request=HttpRequest()
        response=index(request)
        string=response.content.decode("utf8")
        self.assertIn('Hello, Apa kabar?',string)

    def test_story6_using_template(self):
        response=Client().get('/story6/')
        self.assertTemplateUsed(response,'landing_page.html')

    def test_model_can_create_new_status(self):
        new_status=MyForm.objects.create(status='Sedang makan')

        counting_all_available_status =  MyForm.objects.all().count()
        self.assertEqual(counting_all_available_status,1)

    def test_form_status_has_css_classes(self):
         form = StatusForm()
         self.assertIn('class="form-control',form.as_p())

    def test_from_validation_for_blank_field(self):
        form=StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    def test_story6_post_success_and_render_the_result(self):
        test='Ngopi'
        response_post=Client().post('/story6/',{'status':test})
        self.assertEqual(response_post.status_code,200)

        response = Client().get('/story6/')
        self.assertTemplateUsed(response,'landing_page.html')
    
    # Make testcase for story 6 challenge
    def test_challenge_url_is_exist(self):
        response=Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_challenge_using_index_func(self):
        found=resolve('/profile/')
        self.assertEqual(found.func,profile)

class LibraryPageUnitTest(TestCase):
    #Test for library page
    def test_library_url_is_exist(self):
        response=Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_story9_using_books_func(self):
        found=resolve('/books/')
        self.assertEqual(found.func,books)

    def test_story9_return_jsonresponse_quilting(self):
        response = Client().get('/books/books_data/quilting')
        self.assertEqual(response.status_code,301)

class SubscribersPageUnitTest(TestCase):
    #Test for subscriber page
    def test_subscribers_url_is_exist(self):
        response=Client().get('/subscribers/')
        self.assertEqual(response.status_code,200)

    def test_story10_post_success_and_render_the_result(self):
        name='saffa'
        email='saffa@gmail.com'
        password = 'inipassword'
        response_post=Client().post('/Save_Subscribers/',{'name':name,'email':email,'password':password})
        self.assertEqual(response_post.status_code,200)

        response = Client().get('/subscribers/')
        self.assertTemplateUsed(response,'subscribers.html')

    def test_story10_from_validation_for_blank_field(self):
        form=SubscriberForms(data={'name':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    def test_post_error_and_does_not_exists_in_the_database(self):
        Client().post('/Save_Subscribers/',{'name': ''})
        count_data = SubscriberModels.objects.all().count()
        self.assertEqual(count_data, 0)

    def test_JSON_object_failed_created(self):
        create_model = SubscriberModels.objects.create(name='saffa',email='laila@saffa.com',password='inipassword')
        self.assertRaises(IntegrityError)

    #Unit test for story 10 challenge
    def test_challenge10_url_exist(self):
        response=Client().get('/Show_Subscribers/')
        self.assertEqual(response.status_code,200)

    def test_challenge10_object_does_not_exists_in_the_database(self):
        Client().post('/Show_Subscribers/',{'name': ''})
        count_data = SubscriberModels.objects.all().count()
        self.assertEqual(count_data, 0)

    def test_challenge10_subscribers_json_exist(self):
        response = self.client.post('/Show_Subscribers/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'subsdata': []}
        )


#Functional Test for Story 6 PPW 2018
# class Story6FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Story6FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FunctionalTest,self).tearDown()

#     def test_input_status_form(self):
#         selenium=self.selenium
         
#         #Opening web url we want to test
#         selenium.get('https://story-6-saffa.herokuapp.com/story6/')
        
#         #Find form element
#         status=selenium.find_element_by_class_name('form-control')
#         submit=selenium.find_element_by_id('submit-button')
    
#         #Fill form with data
#         status.send_keys('Coba-Coba')

#         #Submitting the form
#         submit.send_keys(Keys.RETURN)
#         #self.assertIn('Coba-Coba',selenium.page_source)
        
#     def test_story6_contain_layout(self):
#         selenium = self.selenium

#         selenium.get('https://story-6-saffa.herokuapp.com/story6/')

#         #Test First Layout
#         hello_h1 = selenium.find_element_by_tag_name('h1').text
#         self.assertIn("Hello, Apa kabar?",hello_h1)


#         #Test Second Layout
#         button_layout = selenium.find_element_by_id('submit-button').text
#         self.assertIn("Submit",button_layout)

#     def test_story6_contain_style(self):
#         selenium = self.selenium
#         selenium.get('https://story-6-saffa.herokuapp.com/story6/')
   
#         fontstyle =selenium.find_element_by_id('hello-text').value_of_css_property('font-style')
#         self.assertIn('italic',fontstyle)
#         fontweight =selenium.find_element_by_id('hello-text').value_of_css_property('font-weight')
#         self.assertIn('700',fontweight)
       

        



