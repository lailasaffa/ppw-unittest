from django.contrib import admin
from .models import MyForm
from .models import SubscriberModels

# Register your models here.
admin.site.register(MyForm)
admin.site.register(SubscriberModels)