from django.urls import path
from .views import index
from .views import profile
from .views import books
from .views import books_data
from .views import subscribers
from .views import save_subscribers
from .views import show_subscribers
from .views import delete_subscribers

app_name = 'story6app'
urlpatterns = [
    path('story6/',index,name='index'),
    path('profile/',profile,name='profile'),
    path('books/',books,name='books'),
    path('books/books_data/<str:tema>/',books_data,name='books'),
    path('subscribers/', subscribers, name='subscribers'),
    path('Save_Subscribers/',save_subscribers, name='Save_Subscribers'),
    path('Show_Subscribers/',show_subscribers,name='Show_Subscribers'),
    path('Delete_Subscribers/',delete_subscribers,name="Delete_Subscribers")
]
