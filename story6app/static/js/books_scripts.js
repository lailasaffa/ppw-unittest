var counter=0;
$(document).ready(function(){
    var clicked=true;
    $('#theme-button').click(function(){
        if(clicked){
            $('body').css('background-color','#569AA7')
            $('tbody').css('background-color','#569AA7')
            $('#header').css('color','#F1A764')
            $('.counter-block').css('color','#F1A764')
            $('th').css('color','#F1A764')
            $('td').css('color','#F1A764')
            clicked=false

        }else{
            $('body').css('background-color','#232223')
            $('tbody').css('background-color','#232223')
            $('#header').css('color','#DBBB5E')
            $('.counter-block').css('color','#DBBB5E')
            $('th').css('color','#DBBB5E')
            $('td').css('color','#DBBB5E')
            clicked=true
        }
    });

    $("#quilting").click(function() {
        proses('quilting');
    });

    $("#architecture").click(function() {
        proses('architecture');
    });
    $("#comic").click(function() {
        proses('comic');
    });
    $(document).on('click', '#honne', function() {
        if( $(this).hasClass('clicked')) {
            counter -=1;
            $(this).css("color","black")
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;
            $(this).css("color","brown")
        }
        $('.counter').html(counter);
    });
});


function proses(txt) {
    $.ajax({
        url:'books_data/' + txt,
        type:'GET',
        dataType:'json',
        success: function(data){
            var row = ('<tr>');
            for(var i = 1;i<data.data.length;i++){
                row+= '<th scope ="col">' + i +'</th>';
                row+= '<td>'+'<img src=\"' + data.data[i-1].thumbnail + '\">'+'</td>';
                row+= '<td>'+data.data[i-1].title+'</td>';
                row+= '<td>'+data.data[i-1].authors +'</td>';
                row+= '<td>'+ data.data[i-1].publishedDate+'</td>';
                row+= '<td>' +'<button type="button" id="honne" class="btn btn-dark"><i class ="fa fa-adjust" style="font-size:24px;color:black"></i></button> </td></tr>';
            }
            $('#book-table').append(row);
        }
    });
}