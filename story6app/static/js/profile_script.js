$(document).ready(function(){
    $('.accordions').find('.accordion').click(function(){

        //Expand or collapse this panel
        $(this).next().slideToggle('fast');
  
        //Hide the other panels
        $(".panel").not($(this).next()).slideUp('fast');
  
      });
    //Change theme
    var clicked = true;
    $('#theme-button').click(function(){  
        if(clicked) {
            //Change theme for profile page
            $("body").css("background-color","#EDF7F6");
            $("#hello").css("color","#2660A4");
            $("#name").css("color","#2660A4");
            $(".description").css("color","black");
            $("#acc-description").css("color","#2660A4");
            $(".accordion").css({"color":"white", "background-color":"#2660A4"});
            $(".panel").css("background-color","black");
            $(".content").css("color","white");
            //Change theme for status page
            $("#id-body.class-body").css("background-color","#6C698D")
            $("#result-status.col-sm-4").css("background-color","#D4D2D5")
            $("#hello-text").css("color","#BFAFA6")
            clicked=false;
        } else {
            //Change theme to original for profile page
            $("body").css("background-color","#93ACB5");
            $("#hello").css("color","white");
            $("#name").css("color","#6C756B");
            $(".description").css("color","white");
            $("#acc-description").css("color","lavender");
            $(".accordion").css({"color":"white", "background-color":"slategray"});
            $(".panel").css("background-color","white");
            $(".content").css("color","slategray");

            //Change theme to original for status page
            $("#id-body.class-body").css("background-color","#D3CBC1")
            $("#result-status.col-sm-4").css("background-color","rosybrown")
            $("#hello-text").css("color","#5A8679")
            clicked=true;
        }
    });
    //Hover foto
    $("#profile-img").tooltip();
});
//Challenge Story 8, loader untuk page
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
document.getElementById("loader").style.display = "none";
document.getElementById("myDiv").style.display = "block";
}

