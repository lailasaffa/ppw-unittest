$(document).ready(function(){
    $('#subs-button').closest('form')
    .submit(function() {
        return checkForm.apply($(this).find(':input')[0]);
    })
    .find(inputSelector).keyup(checkForm).keyup();
  
    $('#subsform').submit(function(e){
        e.preventDefault();
        $.ajax({
            method:'POST',
            url:'/Save_Subscribers/',
            data:$('form').serialize(),
            success:function(status){
                if (status.status == 'True') {
                    $('#buat-alert').html("<div class='alert alert-success' role='alert'>Thank you for subscribing! Have a good day:)</div>");
                }else{
                    $('#buat-alert').html("<div class='alert alert-danger' role='alert'>This email is already been used to subscribe</div>");
                }
            }
    
        });
        return false;
    });
    var clicked=true;
    $('#theme-button').click(function(){
        if(clicked){
            $('body').css('background-image','url("https://scontent.fcgk10-1.fna.fbcdn.net/v/t1.0-9/46516083_2758114107535732_8496331522154954752_n.jpg?_nc_cat=104&_nc_eui2=AeFVa_JXajrT-NckPnq0hvKumUAKAuVwH5zNMDC7B55cbg3Xoplxm4nkVcORy-dJpBR3M2xNAGZYEsW3rzORDv2EvXNjIDCNAu5GdI2YjMKptA&_nc_ht=scontent.fcgk10-1.fna&oh=73a641bb95b148b83543761a1ff746cd&oe=5CB0A010")')
            clicked=false

        }else{
            $('body').css('background-image','url("https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/46521237_2753676037979539_7007799640951095296_o.jpg?_nc_cat=105&_nc_ht=scontent-sin6-1.xx&oh=0ea3e94ca382c4c2b4233604eb5acba0&oe=5CAE80C9")')
            clicked=true
        }
    });

    $.ajax({
        url:'/Show_Subscribers/',
        type:'GET',
        dataType:'json',
        success: function(data){
            var row = ('<tr>');
            for(var i = 0;i<data.subsdata.length;i++){
                row+= '<th scope ="col">' + (i+1) +'</th>';
                row+= '<td scope ="col">' + data.subsdata[i].name +'</t>';
                row+= '<td>'+data.subsdata[i].email+'</td>';
                row+= '<td>' +'<button class="btn btn-default" id="unsubs-button" onClick="deleteSubs('+data.subsdata[i].pk+')"type="submit">Unsubscribe</button></td></tr>';
            }
            console.log(row);
            $('#subs-table').append(row);
        }
    });
    $('#unsubs-button').click(function(){
        
    });
    
    
});

const inputSelector = ':input[Required]:visible';
function checkForm() {
  var isValidForm = true;
  $(this.form).find(inputSelector).each(function() {
    if (!this.value.trim()) {
      isValidForm = false;
    }
  });
  $(this.form).find('#subs-button').prop('disabled', !isValidForm);
  return isValidForm;
}

function deleteSubs(pk){
    console.log(pk)
    $.ajax({
        method:'POST',
        url:'/Delete_Subscribers/',
        data:{'pk':pk},
        success:function(){
            alert("Goodbye!")
            window.location.reload();
        }
    });
}