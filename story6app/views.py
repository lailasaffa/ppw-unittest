from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.http import HttpResponse
from django.http import JsonResponse
from django.db import IntegrityError
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

import urllib.request, json
import requests

from .forms import StatusForm
from .forms import SubscriberForms
from .models import MyForm
from .models import SubscriberModels
from datetime import datetime,date

name = "Laila Saffanah"
major = "Computer Information System"
univ = "Universitas Indonesia"
bio = "Blood Type: A, Scorpio, INFJ-T"
likes = "Love organizing, Cartoon concept enthusiast, Dystopian enthusiast" 

activities= "Majoring in computer science, my daily activity is coding. I haven't read books again lately, eventough it's one of my hobbies. Currently, i'm only able to read poem books such as nikita gill's and rupi kaur books. "
organizations = "Currently i'm officiates in BEM Fasilkom UI department Pengabdian Masyarakat. I'm also a staff of SIWAKNG in event division. I learn many things from joining organizations. Example: Time management"
achievements = "I have few achievements in life. Passing the SBMPTN is one of it. Another one is second lineup in School's Batik Design Competition in SMAN 78 Jakarta Barat"

response={}

def profile(request):
    response={'name':name,'major':major,'univ':univ,'bio':bio,'likes':likes,'activities':activities,'achievements':achievements,'organizations':organizations}
    return render(request,'profile.html',response)
    
def index(request):
    response['formlist'] = StatusForm
    allFormObj = MyForm.objects.all()
    response['StatusForm'] = allFormObj

    form = StatusForm(request.POST or None)
    if(request.method=='POST' and form.is_valid()):
        response['status'] = request.POST['status']
        allForms = MyForm(status=response['status'])
        allForms.save()
        return render(request,'landing_page.html',response)
    else:
        return render(request,'landing_page.html',response)

def books_data(request,tema):
    raw = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + tema).json()
    items=[]
    for subdata in raw['items']:
        data={}
        data['thumbnail'] = subdata['volumeInfo']['imageLinks']['thumbnail'] if 'imageLinks' in subdata['volumeInfo'].keys() else 'Tidak ada'
        data['title'] = subdata['volumeInfo']['title'] 
        data['authors']=", ".join(subdata['volumeInfo']['authors']) if 'authors' in subdata['volumeInfo'].keys() else 'Tidak ada'
        data['publishedDate']= subdata['volumeInfo']['publishedDate'] if 'publishedDate' in subdata['volumeInfo'].keys() else 'Tidak ada'  
        items.append(data)
    return JsonResponse({"data":items})

def books(request):
    html='books.html'
    return render(request,html)

def subscribers(request):
    form = SubscriberForms(request.POST)
    html = 'subscribers.html'
    return render(request,html,{'form':form})

def save_subscribers(request):
    response['form'] = SubscriberForms()
    form = SubscriberForms(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        status = 'True'
        #Store account into subscribers database
        try:
            storedata = SubscriberModels(
            name=response['name'],
            email=response['email'],
            password=response['password']
            )
            storedata.save()
            return JsonResponse({'status':status})
        except IntegrityError as e:
            status = 'False'
            return JsonResponse({'status':status})        
    status = 'False'
    return JsonResponse({'status':status})

def show_subscribers(request):
    data = list( SubscriberModels.objects.all().values('pk','name','email','password'))
    return JsonResponse({'subsdata':data})
    
@csrf_exempt
def delete_subscribers(request):
    if(request.method == 'POST'):
        subsdata = request.POST['pk']
        SubscriberModels.objects.filter(pk=subsdata).delete()
        return HttpResponse({'status':'Success'})
    else:
        return HttpResponse({'status':'Failed to load page'})
